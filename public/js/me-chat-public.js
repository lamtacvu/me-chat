(function( $ ) {
	
	$(function() {
		/**
		 * TEMPLATE
		 */
		
		 // get template from global variable
		var friend = _.template(''+_friend+'');
		var history_conversations = _.template(''+_history_conversations+'');
		var new_active_conversations = _.template(''+_new_active_conversations+'');
		var my_message = _.template(''+_my_message+'');
		var friend_message = _.template(''+_friend_message+'')		
		var empty_message_container = _.template(''+_empty_message_container+'');
		
		var active_conversation;
		var active_person;
		var current_chat_avatar;
		var current_chat_username;
		var idfriendnew;
		var nameval;

		var arrayuser = [];
		if ($("#conversations-chat")) {
			getMyConversations();
		}


		/**
		 * SEND MESSAGE 
		 */

		$('#send-btn').click(function(){
			if ($('#message-text').val().trim())
			sendMessage()
		})

		$('#message-text').keypress(function(e){
			if (e.keyCode==13) {
				if ($('#message-text').val().trim())
				sendMessage()
			}
		})
		/**
		 * REFRESH MESSAGE
		 */
		$('#refresh-message-btn').click(function(){
			offset =0;// reset offset
			getmyMessages($('#active-chat-conversation').val(),0);
		})
		var offset =0;
		$('#load-old-message-btn').click(function(){
			getOldMessages($('#active-chat-conversation').val(), offset) ;
			offset++;

		})
		/**
		 * SEARCH CONVERSATIONS
		 */
		$('#input-search').keyup(function(){
			
			if ($(this).val().length > 0) {
				var search_data = $(this).val();
				const items = $('.chat_ib');
				$.each(items, function (indexInArray, valueOfElement) { 
					if ($(items[indexInArray]).text().indexOf(search_data) !== -1) {					
						$(items[indexInArray]).parent().parent().css("display","block");
					} else $(items[indexInArray]).parent().parent().css("display","none");
					 
				});
				
			} else {
				$('.chat_list').css("display","block");
			} 
		})

		/**
		 * NEW CONVERSATION
		 */
		$('#start-conversation').click(function(){
			if (idfriendnew) {
				
				const datafriend = idfriendnew;				

				getExistConversationWithFriend(datafriend, function(res){

					$("#active-chat-person").val(datafriend);
					$('#chat-name').text(res['user_nicename']);
					
					if (res) {

						active_conversation = res['ID'];
						active_person = (res['friendID']==cuid)?res['ownerID']:res['friendID'];

						setActiveConversation(active_conversation,active_person);
					}else {

						active_person = datafriend;
						
						$(".chat_list").removeClass('active_chat');
						$("#active-chat-conversation").val('')
						$('#messages-history').html(empty_message_container)		
						$('#conversations-chat').prepend(new_active_conversations({ID:null, friendid:datafriend, avatar: res['avatar'], name: res['user_nicename'], datecreated: (new Date().getMonth()+1 )+'-'+new Date().getDate() }));
					}
					$('#new-conversation').modal('hide');
				})

			} else {

			}
		})
		/**
		 * IF CONVERSATION EXIST SET IT ACTIVE
		 * @param {*} cid 
		 * @param {*} friendid 
		 */
		function setActiveConversation(cid, friendid) {
			
			$('#active-chat-person').val(friendid)
			$('.chat_list').removeClass('active_chat');
			$('.chat_list[data-cid="'+cid+'"]').addClass('active_chat');
			$('#conversations-chat').prepend($('.chat_list[data-cid="'+cid+'"]').clone());
			$('.chat_list[data-cid="'+cid+'"]')[1].remove();
			getmyMessages(cid,0);
		}

		/**
		 * ADD NEW CONVERSATION ACTION
		 */
		$('#add-new-conversation').click(function() {
			/* $.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_all_users'},
				success: function( response ){					

					var users = JSON.parse(response);
					$('#friend-all').empty();
					
					users.users.forEach(element => {
						arrayuser[element.ID] = element;
						$('#friend-all').append(friend({ID: element.ID,nicename: element.meta.nicename}))
					});
					$('#new-conversation').modal('show');
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			}); */
		})

		$('#mc_autocomplete-input').keyup(function(e){
			var search = $(this).val();
			$.ajax({
				type: "POST",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_user', 'query': search},				
				success: function (response) {
					if (response) {
						var uss = JSON.parse(response)['users'];
						$('#mc_autocomplete-results').empty();
						$('#mc_autocomplete-results').css("display","block")
						uss.forEach(element => {
							
							$('#mc_autocomplete-results').append(friend({ID: element.ID,nicename: element.user_nicename}))
						});
						$('#mc_autocomplete-results li').click(function(){
							console.log($(this).data('id'))
							idfriendnew = $(this).data('id');
							$('#mc_autocomplete-input').val($(this).text());
							$('#mc_autocomplete-results').empty();
							$('#mc_autocomplete-results').css("display","none")
						});

					}
				}
			});

		})
		
		/**
		 *  GET ALL MY CONVERSATION
		 */

		function getMyConversations() {
			
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_my_conversations'},
				success: function( response ) {					
					
					if (response) {
						var data_history_pp_chat = JSON.parse(response);
						
						if ($("#conversations-chat")&&data_history_pp_chat['success']) {							
							var counter=0;
							data_history_pp_chat['conversations'].forEach((element ,index)=> {								
								counter++;
								if (index==0) {
									$('#conversations-chat').append(new_active_conversations({ID:element.ID, friendid: (element.friendID==cuid)?element.ownerID:element.friendID,avatar: element.avatar,name: element.name,datecreated: element.Datecreate }));
								} else
									$("#conversations-chat").append(history_conversations({ID:element.ID, friendid:(element.friendID==cuid)?element.ownerID:element.friendID,avatar: element.avatar,name: element.name,datecreated: element.Datecreate }));
								if (counter==data_history_pp_chat['conversations'].length) {
									
									$(".chat_list").click(function(){
										var cid = $(this).data('cid');
										$("#active-chat-person").val($(this).data('friendid'))											
										$(".chat_list").removeClass('active_chat');
										$(this).addClass('active_chat');
										// reset offset
										offset =0;
										// get first active conversation
										getmyMessages(cid, 0);
									})
									$("#active-chat-person").val($(".chat_list").first().data('friendid'))

									active_person = (data_history_pp_chat['conversations'][0].friendID==cuid)?data_history_pp_chat['conversations'][0].ownerID:data_history_pp_chat['conversations'][0].friendID
									active_conversation = 	data_history_pp_chat['conversations'][0].ID;

									getmyMessages(data_history_pp_chat['conversations'][0].ID,0);
								}
							});
						}
					}
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}
		/**
		 * GET MESSAGES WITH CONVERSATION ID
		 * @param {*} cid 
		 * @param {*} limit 
		 */
		function getmyMessages(cid, limit) {
			
			$('#active-chat-conversation').val(cid);
			$('#messages-history').html(empty_message_container)
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_my_messages', 'cid' : cid, 'limit': limit | 0 },
				success: function( response ){
					
					var data_messages = JSON.parse(response);
					if (data_messages['success']) {
						if(data_messages['messages'].length) {
							
							data_messages['messages'].forEach((element,index) => {
								
								
								if (element.from_uid==cuid) {
									$(my_message({message: element.content, datecreated: element.Datecreate})).insertBefore('#flag-messages');
								} else {
									current_chat_username = element.name;
									current_chat_avatar  = element.avatar;
									$(friend_message({avatar: element.avatar, name: element.name, message: element.content,datecreated: element.Datecreate })).insertBefore('#flag-messages');
								}
							})
							$("#messages-history").scrollTo("#flag-messages", 1000);
						}
					}
					
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}
		/**
		 * GET CONVERSATION EXIST WITH FRIEND 
		 * @param {*} friendid 
		 * @param {*} callback 
		 */
		function getExistConversationWithFriend(friendid, callback) {
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_friend_message', 'friendid' : friendid },
				success: function( response ){	
					
					var data_conversation = JSON.parse(response);
					if (data_conversation['success']) {
						callback(data_conversation['conversations']);
					}
				}
			})
		}
		/**
		 * GET OLD MESSAGES
		 */
		function getOldMessages(cid, offset) {
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_file_conversation_new', 'cid': cid , 'offset': offset},
				success: function( response ){	
					if (response) {
						var data_conversation = JSON.parse(response);
						var data = data_conversation['data']['Data'];
						data = data.reverse();
						data.forEach((element,index) => {
								
								
							if (element.from_uid==cuid) {
								
								$("#messages-history").prepend(my_message({message: element.content, datecreated: element.Datecreate}));
							} else {
								
								$("#messages-history").prepend(friend_message({avatar: current_chat_avatar, name: current_chat_username, message: element.content,datecreated: element.Datecreate }));
							}
						})
					}
					
				}
			})
		}
		/**
		 * DO SEND MESSAGE
		 */
		function sendMessage() {
			if ($("#active-chat-person").val().trim() || !$('#active-chat-conversation').val()) {

				newConversation(parseInt($("#active-chat-person").val().trim()), function(res){
					if (res) {
						const data_return = JSON.parse(res);
						
						$('#active-chat-conversation').val(data_return['cid'])
						if (data_return['success'])
						sendMessageNow(data_return['cid'], $("#active-chat-person").val().trim(),$('#message-text').val().trim() );
					}
				})
			} else if ($('#active-chat-conversation').val() && $("#active-chat-person").val()) {
				sendMessageNow($('#active-chat-conversation').val().trim(), $("#active-chat-person").val().trim(), $('#message-text').val().trim());
				
			}
		}
		/**
		 * SEND MESSAGE AJAX
		 * @param {*} cid 
		 * @param {*} to_uid 
		 * @param {*} message 
		 */
		function sendMessageNow(cid, to_uid, message){
			
			$('#message-text').val('')
			$(my_message({message: message, datecreated: getDateCreated()})).insertBefore('#flag-messages');

			var dataJSON = {
				'action'	: 'new_messages',
				'cid'		: parseInt(cid),
				'to_uid' 	: parseInt(to_uid),
				'message' 	: message,
				'new_message_nonce' : new_message_nonce
			};
			
			$.ajax({
				cache: false,
				type: "POST",
				url: wp_ajax.ajax_url,
				data: dataJSON,
				success: function( response ){					
					
					$("#messages-history").scrollTo("#flag-messages", 1000);			
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}

		/**
		 * AJAX NEW CONVERSATION
		 * @param {*} friendid 
		 * @param {*} callback 
		 */
		function newConversation(friendid, callback) {
			var dataJSON = {
				'action'	: 'new_conversation',
				'friend_id'		: friendid	,
				'new_conversation_nonce': new_conversation_nonce			
			};
			
			$.ajax({
				cache: false,
				type: "POST",
				url: wp_ajax.ajax_url,
				data: dataJSON,
				success: function( response ){	
							
					if (response) callback(response)
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}
		jQuery.fn.scrollTo = function(elem, speed) { 
			$(this).animate({
				scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top 
			}, speed == undefined ? 1000 : speed); 
			return this; 
		};
		/**
		 * DATE CREATE 
		 */
		function getDateCreated() {
			const today = new Date();
			let dd = today.getDate();
			let mm = today.getMonth() + 1; // January is 0!
			const yyyy = today.getFullYear();
			let h = today.getHours();
			let m = today.getMinutes();
			let s = today.getSeconds();
			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (h < 10) {
				h = '0' + h;
			}
			if (m < 10) {
				m = '0' + m;
			}
			if (s < 10) {
				s = '0' + s;
			}
			// ex : today is 2018-07-30 00:00:00
			return yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + m + ':' + s;
		}
		

	});



})( jQuery );
