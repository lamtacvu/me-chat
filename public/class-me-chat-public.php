<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       wordpress.org
 * @since      1.0.0
 *
 * @package    Me_Chat
 * @subpackage Me_Chat/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Me_Chat
 * @subpackage Me_Chat/public
 * @author     lamvu <tunglam195@gmail.com>
 */
class Me_Chat_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		/**
		 * ERROR IN TIMEZONE WITH XAMPP REMOVE IT LATER
		 */
		// NEED TO REMOVE IT IF PRODUCTION
		ini_set("date.timezone", "Asia/Ho_Chi_Minh");
		date_default_timezone_set('Asia/Ho_Chi_Minh'); 
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Me_Chat_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Me_Chat_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		
		global $post;
		if ( has_shortcode( $post->post_content, 'me_chat') ) {
			wp_enqueue_style( $this->plugin_name.'-bs', '//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', array(), $this->version, 'all' );		}
			wp_enqueue_style( $this->plugin_name.'-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/me-chat-public.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Me_Chat_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Me_Chat_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		global $post;
		if (  has_shortcode( $post->post_content, 'me_chat') ) {
			wp_enqueue_script( $this->plugin_name.'-jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->plugin_name.'-bsmin', '//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->plugin_name.'-under-score', 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js', array( 'underscore' ), $this->version, true );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/me-chat-public.js', array( 'jquery' ), $this->version, true );
			wp_localize_script( $this->plugin_name, 'wp_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

		}

	}
/*################################################# GET ############################################################
*/

	public function getAllUsers() {
		$users = get_users( array( 'fields' => array( 'ID' ) ) );
		$return_data = array();
		$i=0;
		foreach($users as $user_id){
			$return_data[$i]['ID'] =  $user_id->ID;
			$user_meta = get_user_meta ( $user_id->ID);
			$user_info =  get_userdata($user_id->ID);

			$return_data[$i]['meta'] = array( 	'nickname'	=> $user_meta->nickname[0],
											'firstname' => $user_meta->first_name[0],
											'lastname' 	=> $user_meta->last_name[0],
											'nicename'	=> $user_info->user_nicename,
											'email'		=> $user_info->user_email,
											'avatar'	=> get_avatar_url($user_id->ID)

										); 
			$i++;
			
		}
		die(json_encode(array('success' => true, 'users' => $return_data ))) ;
	}


	public function getUsers() {
		$query = $_POST['query'];
		if ($query) {
			global $wpdb;
			$users_table = $wpdb->prefix . 'users';
			$sql = 'SELECT ID, user_nicename FROM '.$users_table.'  WHERE user_nicename LIKE %s ;';
			$results = $wpdb->get_results($wpdb->prepare($sql,  array($wpdb->esc_like($query).'%')));
			die(json_encode(array('success' => true, 'users' => $results ))) ;
		}
		die(null) ;
	}
/**
 * ###########################	 CONVERSATIONS ##############################
 */
	/**
	 * get all conversations of all times
	 */
	public function getAllConversations() {
		global $wpdb;
		$conversations_table = $wpdb->prefix . 'MC_conversations';
		$sql = 'SELECT ID FROM '.$conversations_table.'  ;';
		$results = $wpdb->get_results($sql);
		return $results ;
	}
	/**
	 * get conversations of current user
	 */
	public function getMyConversations() {
		$cuid = get_current_user_id();
		global $wpdb;
		$conversations_table = $wpdb->prefix . 'MC_conversations';
		$users_table = $wpdb->prefix . 'users';
		$sql = 'SELECT a.ID, a.ownerID, a.friendID, a.Datecreate, b.user_nicename as name, b.user_email, b.ID AS userid FROM '.$conversations_table.' a INNER JOIN '.$users_table.' b  WHERE ( a.ownerID = '.$cuid.'  AND a.friendID = b.ID ) OR ( a.friendID = '.$cuid.' AND a.ownerID = b.ID  ) GROUP BY a.ID ;';
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $key => $value) {
				$results[$key]->avatar = get_avatar_url($value->userid);
			}
			die(json_encode(array('success' => true, 'conversations' => $results ))) ;
		}
		die(null);
	}
	/**
	 * 
	 */
	public function getMyConversationId($friendID) {
		$cuid = get_current_user_id();
		global $wpdb;
		$conversations_table = $wpdb->prefix . 'MC_conversations';
		$sql = 'SELECT * FROM '.$conversations_table.' WHERE ( ownerID = %d AND friendID = %d ) OR ( friendID = %d AND ownerID = %d ) ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($cuid, $friendID, $cuid, $friendID)));
		return $results;
	}
	/**
	 * GET CONVERSATION BY SPECIFIC ID OF USER
	 */
	public function getConversationsByUserId() {
		global $wpdb;
		if (isset($_GET) && isset($_GET['friendid'])) {
			$id = $_GET['friendid'];
			$cuid = get_current_user_id();
			$conversations_table = $wpdb->prefix . 'MC_conversations';
			$users_table = $wpdb->prefix . 'users';
			$sql = 'SELECT * FROM '.$conversations_table.' WHERE  ( ownerID = %d AND friendID= %d) OR (friendID= %d AND ownerID=%d );';

			$results_conversation = $wpdb->get_results($wpdb->prepare($sql, array( $id, $cuid, $id, $cuid)))[0];
			if ($results_conversation) {
				
				if ($results_conversation->ownderID==$cuid) {
						$user_info = get_userdata($results_conversation->friendID);
						$results_conversation->avatar = get_avatar_url($results_conversation->friendID);
						$results_conversation->nicename = $user_info->user_nicename;

				} else {
						$user_info = get_userdata($results_conversation->ownderID);
						$results_conversation->avatar = get_avatar_url($results_conversation->ownderID);
						$results_conversation->nicename = $user_info->user_nicename;

					}
				
				die(json_encode(array('success' => true, 'conversations' => $results_conversation ))) ;
			}
			die(null);
		}
		die(null);
	}
	/**
	 * GET CONVERSATION BY CONVERSATION ID
	 */
	public function getConversationsById($id) {
		global $wpdb;
		$conversations_table = $wpdb->prefix . 'MC_conversations';
		$sql = 'SELECT * FROM '.$conversations_table.' WHERE ID = %d  ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($id)));
		return $results;
	}
/**
 * ############################	MESSAGES #################################
 */
	/**
	 * GET ALL MESSAGES OF ALL USER OF ALL TIMES
	 */
	public function getAllMessages() {
		global $wpdb;
		$messages_table = $wpdb->prefix . 'MC_messages';
		$sql = 'SELECT * FROM '.$messages_table.'  ;';
		$results = $wpdb->get_results($sql);
		return $results;
	}	
	/**
	 * GET ALL MESSAGES BY CONVERSATION ID
	 */
	public function getMyMessagesByConversationId() {
		global $wpdb;
		$cuid = get_current_user_id();
		if (isset($_GET) && isset($_GET['cid']))		{
			$id = $_GET['cid'];
			$limit = $_GET['limit'];
			$messages_table = $wpdb->prefix . 'MC_messages';
			$users_table = $wpdb->prefix . 'users';
			$sql = 'SELECT * FROM '.$messages_table.' a   WHERE cid = %d AND (from_uid = %d OR to_uid = %d ) ORDER BY Datecreate DESC LIMIT 10;';
			$results = $wpdb->get_results($wpdb->prepare($sql, array($id, $cuid, $cuid)));
			
			if ($results) {
				$results = array_reverse($results);
				foreach ($results as $key => $value) {
					if ($value->from_uid == $cuid) { 
						$results[$key]->avatar = get_avatar_url($value->to_uid);
						$results[$key]->name = get_userdata($value->to_uid)->user_nicename;
					} else if ($value->to_uid == $cuid) {
						$results[$key]->avatar = get_avatar_url($value->from_uid);
						$results[$key]->name = get_userdata($value->from_uid)->user_nicename;
					}
				}
			}
			die(json_encode(array('success' => true, 'messages' => $results ))) ;
		}
		die(null);
	}
	/**
	 * GET MESSAGE BY ID
	 */
	public function getMyMessagesById($id) {
		global $wpdb;
		$cuid = get_current_user_id();		
		$messages_table = $wpdb->prefix . 'MC_messages';
		$sql = 'SELECT * FROM '.$messages_table.'  WHERE ID = %d AND from_uid = %d  ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($id, $cuid)));
		return $results;
	}
	/**
	 * GET MESSAGE BY RECEIVER ID
	 */
	public function getMyMessageByReceiverId($friendID, $cid) {
		global $wpdb;
		$cuid = get_current_user_id();		
		$messages_table = $wpdb->prefix . 'MC_messages';
		$sql = 'SELECT * FROM '.$messages_table.'  WHERE cid = %d AND from_uid = %d AND to_uid = %d  ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($cid, $cuid, $friendID)));
		return $results;
	}
	/**
	 * GET MY MESSAGES BETWEEN TIME
	 */
	public function getMyMessagesInTime($cid, $fromTime, $toTime) {
		global $wpdb;
		$cuid = get_current_user_id();		
		$messages_table = $wpdb->prefix . 'MC_messages';
		$sql = 'SELECT * FROM '.$messages_table.' WHERE cid = %d AND (from_uid = %d OR to_uid = %d ) AND Datecreate >= %s AND Datecreate <= %s ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($cid, $cuid, $cuid, $fromTime, $toTime)));
		return $results;
	}
	/**
	 * GET ALL MESSAGES IN TIME
	 */
	public function getAllMessagesInTime($cid, $fromTime, $toTime) {
		global $wpdb;
		$messages_table = $wpdb->prefix . 'MC_messages';
		$sql = 'SELECT * FROM '.$messages_table.' WHERE cid = %d  AND Datecreate >= %s AND Datecreate <= %s ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($cid, $fromTime, $toTime)));
		return $results;
	}
/**
 * ############################	FILE  CONVERSATION ###############################
 */
	/**
	 * GET ALL FILES - CONVERSATION RECORD
	 */
	public function getAllFileConversations() {
		global $wpdb;
		$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';
		$sql = 'SELECT * FROM '.$file_conversations_table.' ;';
		$results = $wpdb->get_results($sql);
		return $results;
	}
	/**
	 * GET FILE CONVERSATION BY CONVERSATION ID
	 */
	public function getFileConversationByCid($cid) {
		global $wpdb;
		$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';
		$sql = 'SELECT * FROM '.$file_conversations_table.' WHERE cid= %d ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($cid)));
		return $results;
	}
	
	/**
	 * GET FILE CONVERSATION BETWEEN TIME
	 */
	public function getFileConversationInTime($cid, $fromTime, $toTime) {
		global $wpdb;
		$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';
		$sql = 'SELECT * FROM '.$file_conversations_table.' WHERE cid=%d AND Datecreate >= %s AND Datecreate <= %s ;';
		$results = $wpdb->get_results($wpdb->prepare($sql, array($cid, $fromTime, $toTime)));
		return $results;
	}
	/**
	 * 
	 */
	public function getFileConversationNew() {
		$cid = $_GET['cid'];
		$offset = $_GET['offset'];
		if ($cid) {
			global $wpdb;
			$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';
			$sql = 'SELECT * FROM '.$file_conversations_table.' WHERE cid=%d  ORDER BY ID DESC  LIMIT 1 OFFSET %d;';
			$results = $wpdb->get_results($wpdb->prepare($sql, array($cid, $offset)));
			if ($results[0]) {
				$filename = ($results[0]->filename);
				if (file_exists(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/data_cronjob/'.md5($cid).'/'.$filename)) {

					$filepath = WP_PLUGIN_DIR . '/' . $this->plugin_name . '/data_cronjob/'.md5($cid).'/'.$filename;
					$data = json_decode(file_get_contents($filepath));
					
					die(json_encode(array('success' => true, 'data' => $data ))) ;
				};
			}
			die(null);
		}
		die(null);
	}

/**
 * ############################################ CREATE ##############################################################
 */
	/**
	 * CREATE NEW CONVERSATION
	 */
	public function addConversation( ) {
		// By default, check_ajax_referer dies if nonce can not been verified
		if( ! check_ajax_referer( 'new-conversation-nonce', 'new_conversation_nonce', false ) ) {
			wp_send_json_error();
		} else {

			global $wpdb;
			$ownerID 	= get_current_user_id();
			$friendID 	= $_POST['friend_id'];
			$conversation_result = $this->getMyConversationId($friendID);
			if ($conversation_result) {
				die(json_encode(array('success' => true, 'cid' => $conversation_result[0]->ID ))) ;
			} else {

				$conversations_table = $wpdb->prefix . 'MC_conversations';		
				$results = $wpdb->insert($conversations_table, 
										array(
											'ownerID' 	=> $ownerID, 
											'friendID' 	=> $friendID
										)
									);
				if ($results) {
					die(json_encode(array('success' => true, 'cid' => $wpdb->insert_id ))) ;
				}
			}
			die(null);
		}
		die(null);
		
	}
	/**
	 * INSERT NEW MESSAGE
	 */
	public function addMessages() {
		if( ! check_ajax_referer( 'new-message-nonce', 'new_message_nonce', false ) ) {
			wp_send_json_error();
		} else {

			global $wpdb;
			$from_uid 	= get_current_user_id();
			$cid 		= $_POST['cid'];
			$to_uid 	= $_POST['to_uid'];
			$message 	= $_POST['message'];

			$conversation_result = $this->getMyConversationId($to_uid);
			if ($cid==$conversation_result[0]->ID) {
				if (trim($message)) {

					$messages_table = $wpdb->prefix . 'MC_messages';		
					$results = $wpdb->insert($messages_table, 
											array(
												'cid' 		=> $cid,
												'from_uid' 	=> $from_uid, 
												'to_uid' 	=> $to_uid,
												'content'	=> $message
											)
										);
					if ($results) {
						die(json_encode(array('success' => true, 'mess_id' => $wpdb->insert_id ))) ;
					}
				}
				die(null);
			}
			die(null);
		}
		die();
	}
	/**
	 * INSERT TO FILE CONVERSATION 
	 */
	public function addFileConversation() {
		global $wpdb;
		
		$fromTime = date("Y-m-d H:i:s", strtotime("-1 days"));
		$toTime = date("Y-m-d H:i:s");
		$all_conversations = $this->getAllConversations();
		/**
		 * GET ALL DATA FROM DATABASE
		 */
		$data_files = array();
		$conversation = array();
		if ($all_conversations && count($all_conversations)) {
			foreach ($all_conversations as $key => $value) {
				$cid = $value->ID;
				$conversation = $this->getAllMessagesInTime($cid, $fromTime, $toTime);
				$length_data = count($conversation);
				$data_files[$cid] = array(
									'cid'						=> $cid, 
									'file_created_datetime' 	=> date("Y-m-d H:i:s"),
									'length' 					=> $length_data,
									'Data' 						=> $conversation
				);
			}
		}
		/**
		 * INSERT DATA TO FILES
		 */
		$filenames = array();
		if ($data_files) {
			foreach ($data_files as $key => $value) {
				try{
					if ($value['length']&&$value['length']>10) {
						if (!file_exists(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/data_cronjob/'.md5($key))) {
								// folder cid not exist -> create folder
								mkdir(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/data_cronjob/'.md5($key), 0777, true);
						} 
						$filename = time().'.json';
						$filenames[] = array($key,$filename);
						$newfile = fopen(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/data_cronjob/'.md5($key).'/'.$filename, 'w') or die("Can't create file");
						fwrite($newfile, json_encode($value));
						fclose($newfile);
					}
				} catch(Exception $e) {

				}				
			}
		}
		/**
		 * INSERT DATA TO DATABASE WITH FILES CREATED
		 */
		$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';

		if (count($filenames)) {
			foreach ($filenames as $key => $value) {
				try {
					$results = $wpdb->insert($file_conversations_table, 
									array(
										'cid' 		=> $value[0],
										'filename' 	=> $value[1]
									)
								);
				} catch(Exception $e) {

				}
			}
		}

		/**
		 * DELETE DATA AFTER INSERTED
		 */
		if ($conversation&&(count($conversation)>10)) {
			foreach ($conversation as $key => $value) {
				deleteMessagesInTime($value->ID);
			}
		}

	}
/**
 * ######################################### UPDATE ################################################################
 * 
 */
	/**
	 * UPDATE CONVERSATION 
	 */
	public function updateConversation($id,  $friendID) {
		global $wpdb;
		$conversations_table = $wpdb->prefix . 'MC_conversations';
		$ownerID = get_current_user_id();
		$conversation_result = $this->getMyConversationId($friendID);
		if ($id == $conversation_result[0]->ID) {
			$rows_updated = $wpdb->update($conversations_table, 
									array(
										'ownerID' 	=> $ownerID, 
										'friendID' 	=> $friendID
									),
									array(
										'ID' 		=> $id
									)
								);
			die(json_encode(array('success' => true, 'rows_updated' => $rows_updated ))) ;
				
		}
		die(null);
	}
	/**
	 * UPDATE MESSAGES
	 */
	public function updateMessage() {
		global $wpdb;
		$id = $_POST['id'];
		$cid = $_POST['cid'];
		$from_uid = get_current_user_id();
		$to_uid = $_POST['to_uid'];
		$content = $_POST['content'];
		$conversation_result = $this->getMyConversationId($to_uid);
		if ($cid== $conversation_result[0]->ID) {
			$message_result = getMyMessageByReceiverId($to_uid, $cid);
			if ($id == $message_result[0]->ID) {
				$messages_table = $wpdb->prefix . 'MC_messages';
				$rows_updated = $wpdb->update($messages_table, 
										array(										
											'$content'	=> $content
										),
										array(
											'ID' 		=> $id
										)
									);
				die(json_encode(array('success' => true, 'rows_updated' => $rows_updated ))) ;
				
			}
			die(null);
		}
		die(null);
	}
	/**
	 * UPDATE FILE CONVERSATION DATA
	 */
	public function updateFileConversations($id, $cid, $filename) {
		global $wpdb;
		$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';		
		$rows_updated = $wpdb->update($file_conversations_table, 
								array(
									'cid' 	=> $cid, 
									'filename' 	=> $filename
								),
								array(
									'ID' 		=> $id
								)
							);
		return $rows_updated;
	}

/**
 * ###################################### DELETE ####################################################################
 * 
 */
	/**
	 * DELETE CONVERSATION
	 */
	public function deleteConversation($id) {
		global $wpdb;
		$conversation_result = $this->getConversationsById($id);
		if ($id == $conversation_result[0]->ID) {

			$conversations_table = $wpdb->prefix . 'MC_conversations';	
			$result = $wpdb->delete( $conversations_table, array( 'ID' => $id ) );
			die(json_encode(array('success' => true, 'rows_updated' => $result ))) ;			
		}
		die(null);
	}
	/**
	 * DELETE MESSAGE
	 */
	public function deleteMessage($id) {
		global $wpdb;
		$message_result = $this->getMyMessagesById($id);
		if ($id == $message_result[0]->ID) {

			$messages_table = $wpdb->prefix . 'MC_messages';		
			$result = $wpdb->delete( $messages_table, array( 'ID' => $id ) );
			die(json_encode(array('success' => true, 'rows_updated' => $result ))) ;
		}
		die(null);
	}
	public function deleteFileConversation($id) {
		global $wpdb;
		$file_conversations_table = $wpdb->prefix . 'MC_file_conversations';		
		$result = $wpdb->delete( $file_conversations_table, array( 'ID' => $id ) );
		return $result;
	}
	public function deleteMessagesInTime($id){
		global $wpdb;
		$message_result = $this->getMyMessagesById($id);
		if ($id == $message_result[0]->ID) {

			$messages_table = $wpdb->prefix . 'MC_messages';		
			$result = $wpdb->delete( $messages_table, array( 'ID' => $id  ) );
			return $result;			
		}
		return null;

		
	}

/**
 * ###################### TEMPLATE PART ###########################################
 */

	public function getTemplate() {
		$templates_dir = 'templates';
		$name = $_GET['name'];
		if ($name){
			die(json_encode(array('success' => true, 'content' => $this->getTemplatePart($name) ))) ;			
			
		}
		die(null);
	}

	public function getTemplatePart($name) {
		$templates_dir = 'templates';
		if (file_exists(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/' . $templates_dir . '/'.$name.'.html')) {
			return file_get_contents(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/' . $templates_dir . '/'.$name.'.html');
		}
	}


	/**
	 * SHORT CODE
	 */
	public function meChatShortcodehandle() {
		if (is_user_logged_in()) {
		$templates_dir = 'templates';
		
		echo '<script> var cuid = '.get_current_user_id().';var new_conversation_nonce = "'. wp_create_nonce( "new-conversation-nonce" ).'" ; var new_message_nonce = "'.wp_create_nonce("new-message-nonce").'"  ; </script>';

		echo '<script> ';
		echo 'var _friend = "'.$this->getTemplatePart('friend_option').'" ;';
		echo 'var _history_conversations = "'.$this->getTemplatePart('history_conversations').'" ;';
		echo 'var _new_active_conversations = "'.$this->getTemplatePart('new_active_conversations').'" ;';
		echo 'var _my_message = "'.$this->getTemplatePart('my_message').'" ;';
		echo 'var _friend_message = "'.$this->getTemplatePart('friend_message').'" ;';
		echo 'var _empty_message_container = "'.$this->getTemplatePart('empty_message_container').'" ;';
		echo '</script>';
		include WP_PLUGIN_DIR . '/' . $this->plugin_name . '/' . $templates_dir . '/chat.php';
		//echo file_get_contents(WP_PLUGIN_DIR . '/' . $this->plugin_name . '/' . $templates_dir . '/chat.php');
		} else wp_redirect(wp_login_url());
	}
	/**
	 * CRONJOB
	 */
	/**
	 * schedule
	 */
	public function meChatScheduleCronjobs(){
		if ( !wp_next_scheduled( 'mechat_cronjobs' ) )
    	wp_schedule_event(time(), 'daily', 'mechat_cronjobs');
	}
	/**
	 * do action in cronjob
	 */
	public function domeChatCronjobs() {
		// do something like backup db
		$this->addFileConversation();
	}
}
