
<div class="container" id="me-chat-container">
    <h3 class="text-center">Messaging</h3>
    <div class="mc_messaging">
            <div class="mc_inbox_msg">
                <div class="mc_inbox_people">
                <div class="mc_headind_srch">
                    <div class="mc_recent_heading">
                    <h4><?php _e('Recent Conversations', 'me-chat')?></h4>
                    
                    <button type="button" id="add-new-conversation" data-toggle="modal" data-target="#new-conversation"   class="btn btn-sm btn-icon btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>

                    </div>
                    <div class="mc_srch_bar">
                    <div class="stylish-input-group">
                        <input type="text" class="search-bar"  placeholder="<?php _e('Search', 'me-chat')?>" id="input-search">
                        <span class="input-group-addon">
                        <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                        </span> </div>
                    </div>
                </div>
                <div class="mc_inbox_chat" id="conversations-chat">
                    
                </div>
                </div>
                <div class="mc_mesgs">
                    <div >
                        
                        <button type="button" id="refresh-message-btn" class="btn btn-info btn-icon pull-right"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                        <button type="button" id="load-old-message-btn" class="btn btn-info btn-icon pull-right"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        
                    </div>
                    <div class="mc_msg_history" id="messages-history">

                        <div>
                            <span class="pull-right" id="chat-name"></span>
                        </div>
                        <!-- incoming messages -->
                        
                        <!-- out going messages -->
                      
                        <div id="flag-messages"></div>
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <input type="text" class="write_msg" id="message-text" placeholder="<?php _e('Type a message', 'me-chat')?>" />
                            <button class="msg_send_btn" type="button" id="send-btn"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
  
        </div>
    </div>
    <!-- Button trigger modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="new-conversation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><?php _e('New Conversation', 'me-chat')?></h4>
            <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
           
            <div class="form-group">
                <label for=""><?php _e('Select Friend', 'me-chat')?></label>
                <div id="mc_autocomplete-container">  
                <input type="text" autofocus="true" class="form-control" name="mc_autocomplete-input" placeholder="<?php _e('Search Friend', 'me-chat')?>" id="mc_autocomplete-input">
                <ul id="mc_autocomplete-results">
                </ul>
                </div>
                
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'me-chat')?></button>
            <button type="button" class="btn btn-primary" id="start-conversation"><?php _e('Start Conversation', 'me-chat')?></button>
        </div>
        </div>
    </div>
    </div>
    <input type="hidden" value="" id="active-chat-person">
    <input type="hidden" value="" id="active-chat-conversation">

